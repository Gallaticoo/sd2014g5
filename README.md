**Problemas Clássicos: Leitores e Escritores**

Problema:
Suponha que existe um conjunto de processos que compartilham 
um determinado conjunto de dados (ex: um banco de dados). 
  Existem processos que lêem os dados (mas não os apagam!)
  Existem processos que escrevem os dados

Análise do problema
Se dois ou mais leitores acessarem os dados simultaneamente não 
há problemas
  E se um escritor escrever sobre os dados?
  Podem outros processos estarem acessando simultaneamente os 
mesmos dados?

**Leitores e Escritores: Solução possível (1)**
  Os escritores podem apenas ter acesso exclusivo aos dados 
compartilhados
  Os leitores podem ter acesso aos dados compartilhados 
Simultaneamente1. 
 
//número de leitores ativos
int rc
//protege o acesso à variável rc
Semaphore mutex
//Indica a um escritor se este 
//pode ter acesso aos dados
Semaphore db
//Incialização: 
mutex=1, 
db=1,
rc=0 


**Escritor**
while (TRUE)
...
//writing is 
//performed
...
...


**Leitor**
while (TRUE)
down(mutex);
rc++;
if (rc == 1)
down(db);
up(mutex);
...
//reading is 
//performed
...
down(mutex);
rc--;
if (rc == 0)
up(db);
up(mutex);

**Escritor**
while (TRUE)
down(db);
...
//writing is 
//performed
...
up(db); 


**Leitores e Escritores: Solução possível (2)**
  A solução proposta é simples mas pode levar à starvation
do escritor
  Exercício
  Desenvolver uma solução que atenda os processos pela ordem de 
chegada, mas dando prioridade aos escritores
  Dica: quando existir um escritor pronto para escrever, este tem 
prioridade sobre todos os outros leitores que cheguem após ele 
querendo ler os dados


rcount 		//Número de leitores
wcount	          //Número de escritores, apenas um escritor de cada vez pode ter acesso aos 
//dados compartilhados
mutex_rcount		 // Protege o acesso à variável rcount
mutex_wcount		 //Protege o acesso à variável wcount
mutex		 //Impede que + do que 1 leitor tente entrar na região crítica
w_db		 //Indica a um escritor se este pode ter acesso aos dados
r_db		 //Permite que um processo leitor tente entrar na sua região crítica

rcount      //Número de leitores
wcount    //Número de escritores, apenas um escritor de cada vez pode ter acesso aos 
               //dados compartilhados
mutex_rcount     // Protege o acesso à variável rc
mutex_wcount   //Protege o acesso à variável wc
mutex        //Impede que + do que 1 leitor tente entrar na região crítica
w_db        //Indica a um escritor se este pode ter acesso aos dados
r_db          //Permite que um processo leitor tente entrar na sua região crítica


**Inicialização**
rcount = 0
wcount = 0
//semáforos
mutex_rcount = 1
mutex_wcount = 1
mutex = 1
w_db = 1
r_db = 1


**Escritor**
while (TRUE){
down(mutex_wcount);
wcount++;
if (wcount == 1)
down(r_db);
up(mutex_wcount);
down(w_db)
…
//Escrita
…
up(w_db)
down(mutex_wcount);
wcount--;
if (wcount == 0)
up(r_db);
up(mutex_wcount);
}




**Leitor**
while (TRUE){
down(mutex);
down(r_db);
down(mutex_rcount);
rcount++;
if (rcount == 1)
down(w_db);
up(mutex_rcount);
up(r_db);
up(mutex);
…
//Leitura dos dados
…
down(mutex_rcount);
rcount--;
if (rcount == 0)
up(w_db);
up(mutex_rcount); )